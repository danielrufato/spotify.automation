
#language:pt
Funcionalidade: Autenticação
    @forgot_senha
    Cenario: Esqueci a senha
        Dado que esteja no login
        Quando acessar esqueceu sua senha
        E informar o e-mail "teste@email.com"
        Então deverá exibir a mensagem "Foi mandado um e-mail para você com instruções de como redefinir sua senha. "