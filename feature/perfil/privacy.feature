#language:pt

Funcionalidade: Perfil
    @privacy
    Cenario: Configurações de Privacidade
        Dado que esteja no Perfil
        Quando clicar nas opçoes de privacidade desejadas
        Então deverá exibir a mensagem "Tem certeza?"