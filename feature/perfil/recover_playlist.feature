#language:pt

Funcionalidade: Perfil
    @notify
    Cenario: Notificação
        Dado que esteja no Perfil
        Quando configurar as notificações desejadas
        Então deverá exibir "Notificações Salvas"