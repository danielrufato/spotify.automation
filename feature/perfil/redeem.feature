#language:pt

Funcionalidade: Perfil
    @privacy
    Cenario: Configurações de Privacidade
        Dado que esteja no Perfil
        Quando aplicar um "código" de cartão Pré-pago
        Então o valor deverá ser creditado para pagamento do plano