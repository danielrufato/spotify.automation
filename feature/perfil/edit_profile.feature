#language:pt

Funcionalidade: Perfil
    @edit_profile
    Cenario: Editar conta
        Dado que esteja no Perfil
        Quando editar os dados da conta
        Então deverá exibir a mensagem "Perfil salvo"