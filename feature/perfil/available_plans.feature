#language:pt

Funcionalidade: Perfil
    @available_plans
    Cenario: Planos Disponiveis
        Dado que esteja no Perfil
        Quando entrar em Planos Disponiveis
        E selecionar um plano
        Então deverá exibir o checkout