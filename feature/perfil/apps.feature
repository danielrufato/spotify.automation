#language:pt

Funcionalidade: Perfil
    @apps   
    Cenario: Aplicativos
        Dado que esteja no Perfil
        Quando entrar em aplicativos
        Então deverá exibir os dispositivos conectados
        E permitir exclui-los