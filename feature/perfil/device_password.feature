#language:pt

Funcionalidade: Perfil
    @device_pass
    Cenario: Senha de Dispositivos
        Dado que esteja no Perfil
        Quando solicitar o cadastro de uma senha para os dispositivos
        Então deverá exibir "Um email foi enviado para configurar sua senha"