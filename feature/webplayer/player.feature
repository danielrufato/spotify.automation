#language:pt
Funcionalidade: Player
Dado que esteja no webplayer
            @music_play
            Cenário: Reproduzir música
                Quando clicar em play
                Então o nome da musica deverá aparecer no player
            @music_pause
                Cenário: Pausar música
                Quando clicar em pause
                Então a música deverá pausar 
            @music_advance
                Cenário: Avançar música
                Quando clicar em avançar
                Então o nome da próxima música deverá aparecer no player
            @music_back
                Cenário: Retroceder música
                Quando clicar em retroceder
                Então o nome da música anterior deverá aparecer no player
            @music_pause
            @select_disp
                Cenário: Selecionar dispositivo de reprodução
                Quando selecionar o dispositivo desejado
                Então o dispositivo deverá aparecer selecionado
            @show_queue
                Cenário: Exibir lista de reprodução
                Quando clicar em Lista
                Então deverá exibir as proximas musicas em ordem de reprodução
