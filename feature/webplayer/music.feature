#language:pt
Funcionalidade: Selecionar música
    Contexto:
        Dado encontrada a música
            @music_play
            Cenário: Reproduzir música
                Quando clicar em play
                Então o nome da musica deverá aparecer no player
            @add_to_playlist
                Cenário: Adicionar música à playlist
                Quando Adicionar a música à uma playlist 
                Então deverá aparecer a mensagem "Música adicionada à playlist"
            @music_queue
                Cenário: Adicionar música à fila
                Quando adicionar a música à fila de reprodução
                Então deverá aparecer a mensagem "Música adicionada à fila"
            @liked_music
                Cenário: Curtir música
                Quando curtir a música
                Então deverá aparecer a mensagem "sua música foi adiconada às músicas curtidas"                              