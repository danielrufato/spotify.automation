#language:pt
Funcionalidade: Selecionar Playlist
    Contexto:
        Dado que esteja no webplayer
            @playlist_home
            Cenário: selecionar da Home
                Quando selecionar a playlist "melhores do calcinha preta" 
                Então deverá ser exibido a playlist com suas músicas
            @playlist_select
                Cenário: selecionar por Gênero
                Quando selecionar genero "Rock" 
                E selecionar "Rock Classico"
                Então deverá ser exibido a playlist com suas músicas
            @playlist_search
                Cenário: selecionar Buscando
                Quando encontrar por uma playlist
                Então deverá ser exibido a playlist com suas músicas