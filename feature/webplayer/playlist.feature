#language:pt

Funcionalidade: playlists
    Contexto:
        Dado que esteja no webplayer
            @create_playlist
            Cenário: criar playlist
                Quando criar uma playlist
                Então deverá aparecer a mensagem "Está um pouco vazio por aqui..."
            @delete_playlist
            Cenário: Deletar playlist
                Quando deletar uma playlist
                Então deverá aparecer a mensagem "Removida de sua livraria"
            @reproduzir_playlist
            Cenário: reproduzir playlist
                Quando reproduzir uma playlist
                Então deverá reproduzir uma música
