#language:pt
Funcionalidade: Selecionar música
    Contexto:
        Dado que esteja no webplayer
            @music_search
            Cenário: selecionar de busca
                Quando buscar por uma música
                E selecionar uma música dentro dela
                Então a música deverá ser reproduzida
            @music_select
                Cenário: selecionar de playlist
                Quando encontrar uma playlist
                E selecionar uma música dentro dela
                Então a música deverá ser reproduzida 
            @music_liked
                Cenário: selecionar de coleção
                Quando entrar em Músicas Curtidas
                E selecionar uma música dentro dela
                Então a música deverá ser reproduzida