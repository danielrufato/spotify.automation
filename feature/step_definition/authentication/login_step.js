const assert = require('assert');
const { Given, When, Then , After } = require('cucumber');
const { Builder, By } = require("selenium-webdriver");

var driver = new Builder().forBrowser("firefox").build();

this.Given('que estou no login', async function () {
    await this.driver.get('https://orgames.com.br/account/login/');
});

this.When('realizar login', async function () {

    await this.driver.findElement(By.name('email')).sendKeys('email');
    await this.driver.findElement(By.name('password')).sendKeys('senha', Key.RETURN);
});

this.Then('Deverá exibir a mensagem "Olá!"', async function () {
  //  var user = await this.driver.findElement(By.className('customer')).getText();
  var promise = assert.equal(driver.findElement(By.className('customer')).getText(),'Olá!'); 
   return promise
});

this.After(async function() {
    this.driver.close();
});
